const mongoose=require('mongoose');
const schema=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    desc:{
        type:String,
        required:true
    },
    genre:{
        type:String,
        
    }
})
const bookdb=mongoose.model('bookdb',schema);
module.exports=bookdb