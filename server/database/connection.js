const mongoose=require('mongoose');

const connDB=async()=>{
    try{
        const con= await mongoose.connect(process.env.MONGO_URL,{
            useNewUrlParser: true,
          useUnifiedTopology: true,
        })
        console.log(`mongo connection to :${con.connection.host}`)
    }catch(err){
        console.log(err)
        process.exit(1)
    }
}
module.exports=connDB
