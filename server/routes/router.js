const express=require('express')
const rout=express.Router()
const services=require('../services/render')
const controller=require('../controller/controller')
rout.get('/',services.homepage)
rout.get('/add-book',services.addbook)
rout.get('/update-book',services.updatebook)

//api
rout.post('/api/book',controller.create)
rout.get('/api/book',controller.find)
rout.put('/api/book/:id',controller.update)
rout.delete('/api/book/:id',controller.delete)
module.exports=rout