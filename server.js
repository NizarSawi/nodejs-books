
const express=require('express')
const app=express();
const PORT=process.env.PORT||3000
const dotenv=require("dotenv")
dotenv.config({path:'config.env'})
const morgan=require('morgan')
const bodyparser=require('body-parser')
const path=require('path')
const connDB=require('./server/database/connection')
const passport = require('passport')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
const config = require('./config')
//log request
app.use(morgan('tiny'))

//DB connection
connDB();

//parser request
app.use(bodyparser.urlencoded({extended:true}))
//
//require('./server/authentication').init(app)

//app.use(session({
  //store: new RedisStore({
   // url: config.redisStore.url
  //}),
  //secret: config.redisStore.secret,
  //resave: false,
  //saveUninitialized: false
//}))

//app.use(passport.initialize())
//app.use(passport.session())
//set view engin
app.set("view engine","ejs")
//app.use("views",path.resolve(__dirname,"views/ejs"))
//loading assets
app.use('/css',express.static(path.resolve(__dirname,"assets/css")))
app.use('/js',express.static(path.resolve(__dirname,"assets/js")))
//app.use("img",express.static(path.resolve(__dirname,"assets/img")))

app.use('/',require('./server/routes/router'))


app.listen(PORT,()=>{console.log(`server is running on http://localhost:${PORT}`)})